from django.shortcuts import render
from django.http import HttpResponse
from .models import Post
from django.views.generic import ListView,DetailView,CreateView,UpdateView,DeleteView

def home(request):
    posts = Post.objects.all()
    context = { 'title': 'Welcome' , 'posts':posts}
    return render(request,'blog/home.html',context)

def about(request):
    context = {'user': 'Tamim', 'title': 'Welcome'}
    return render(request,'blog/about.html',context)

class PostListView(ListView):
    model = Post
    template_name='blog/home.html'
    context_object_name='posts'
    ordering = ['-date_posted']

class PostDetailView(DetailView):
    model = Post

class PostCreateView(CreateView):
    model = Post
    fields = ['title' , 'content']

    def form_valid(self, form):
        form.instance.author= self.request.user
        return super().form_valid(form)
    
