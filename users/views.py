from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import UserRegistrationForm,ProfileUpdateForm,UserUpdateForm
from django.contrib import messages 
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

def register(request):
    if(request.method == "POST"):
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,'User Created')
            return redirect('user-login')
    else:
        form = UserRegistrationForm()
        context ={'form':form}
        return render(request,'users/register.html', context)

@login_required
def Profile(request):
    if request.method =='POST':
        u_form= UserUpdateForm(request.POST, instance=request.user)
        p_form= ProfileUpdateForm(request.POST, request.FILES, instance = request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            print('---',u_form)
            u_form.save()
            p_form.save()
            messages.success(request,'Sccessfull')
            return redirect('user-profile')
    else:    
        u_form= UserUpdateForm(instance=request.user)
        p_form= ProfileUpdateForm(instance = request.user.profile)
    context={'u_form': u_form,'p_form': p_form}
    return render(request,'users/profile.html', context)

